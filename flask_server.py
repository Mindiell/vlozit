#encoding: utf-8
from flask import Flask, render_template, redirect, request, url_for

from vlozit import Vlozit

app = Flask(__name__)
app.config.from_object('config')
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True
app.jinja_env.lstrip_blocks = True

# ================================================
# Main
# ================================================
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/send', methods=['POST'])
def send():
    """
    Sending a paste via the web interface
    """
    return redirect(Vlozit().create_paste(request.form['paste']))

@app.route('/p')
def paste():
    # Get first and only key arg to use as paste identifier
    for key in request.args.keys():
        template = Vlozit().get_template(key)
        if template is not None:
            return render_template(template)
    return redirect('/')

@app.route('/robots.txt')
def robots():
    return 'User-agent: *\nDisallow: /'


# ================================================
# Launching
# ================================================
if __name__ == '__main__':
    app.run(debug=app.config['DEBUG'], host=app.config['HOST'], port=app.config['FLASK_PORT'])
