#encoding: utf-8
import signal
import socketserver

import config
from vlozit import Vlozit

class TimeoutError(Exception):
    pass

class MyHandler(socketserver.BaseRequestHandler):
    def handle(self):
        self.data = b''
        signal.signal(signal.SIGALRM, self.stop_receive)
        self.get_message()

    def stop_receive(self, signum, frame):
        raise TimeoutError()

    def get_message(self):
        while True:
            signal.alarm(1)
            try:
                self.data += self.request.recv(1024)
            except TimeoutError:
                try:
                    url = Vlozit().create_paste(str(self.data, "utf-8"))
                    self.request.sendall(url+b'\n')
                except:
                    self.request.sendall(b'Error while reading datas.\n')
                return


if __name__=="__main__":
    server = socketserver.TCPServer((config.HOST, config.NETCAT_PORT), MyHandler)
    server.serve_forever()
