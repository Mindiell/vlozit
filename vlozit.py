#encoding: utf-8

from Crypto.Hash import SHA
import os
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import guess_lexer, TextLexer
import time

import config

class Vlozit():
    def get_template(self, filename):
        if os.path.exists(self.get_path(filename)):
            return (os.path.sep).join(("pastes",filename[0:2],filename[2:4],filename[4:6],filename))

    def get_path(self, filename):
        return (os.path.sep).join(("templates/pastes",filename[0:2],filename[2:4],filename[4:6],filename))

    def create_paste(self, data):
        # Idées possibles :
        #
        # * Chiffrer le contenu, la clef est dans l'url (c'est donc plutôt pour le serveur)
        #       Utilisation de pycrypto (https://pypi.python.org/pypi/pycrypto)
        # * Permettre de discuter du contenu
        # * Permettre de modifier le contenu (en-dessous) et donc d'en reposter un facilement
        while True:
            filename = SHA.new(bytes(str(time.perf_counter()),"utf-8")).hexdigest()
            path = self.get_path(filename)
            if not os.path.exists(path):
                break
        # Create path if not yet existing
        os.makedirs(os.path.dirname(path))
        # Format url to return
        url = "%sp?%s" % (config.WEBSITE, filename)
        # Guessing which lexer to use
        try:
            lexer = guess_lexer(data)
        except:
            lexer = TextLexer()
        # Converting data via pygments
        colored = highlight(data, lexer, HtmlFormatter(linenos=True))
        # Replace specific characters in order to keep clean
        data = data.replace("#", "&num;").replace("%", "&percnt;").replace("<", "&lt;").replace(">", "&gt;")
        colored = colored.replace("#", "&num;").replace("%", "&percnt;")
        # Write file based on skeleton
        with open("templates/skeleton.html") as f:
            html = f.read()
        html = html.replace("##FORMATTED##", colored)
        html = html.replace("##RAW##", data)
        html = html.replace("##URL##", url)
        with open(path, "w") as f:
            f.write(html)
        return bytes(url, "utf-8")
