VLOZIT
======

Version 0.1

Vlozit (insert, paste in slovak) is a simplistic, open source online pastebin.


Easy to paste, easy to share
----------------------------

You can use Vlozit as a simple online pastebin, as all other pastebin all over the world.

But you can use it throught netcat, which give you a powerful and painless tool :

    ls -l /my/folder |nc vloz.it 8080

Or for a configuration file :

    cat /etc/application/application.conf |nc vloz.it 8080

No more erroneous copy-paste via a terminal where you can't get all the content in one paste !

And whenever you use netcat, server returns the url you can share to everyone :

    user@computer:~$ cat /etc/application/application.conf |nc vloz.it 8080
    http://vloz.it/p?1c26fbe552339686983d25f4253ff082666131d5


Simplifying your pastes
-----------------------

A script has been made to simplify your pastes. You just have to get it and copy it to your /usr/bin
folder :

    sudo cp vlozit /usr/bin/vlozit

Then any paste will be as simple as :

    user@computer:~$ cat /etc/application/application.conf |vlozit

